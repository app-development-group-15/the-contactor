# The Contactor - Group 15
## How to run
Please make sure to install all packages with
```bash
npm install
```
and run the expo environment with
```bash
expo start
```
### List of extra functionality
- Delete all contacts
- Import contacts from local phone
- Camera functionality for editing and creating contacts
- Calling a contact from the contact details screen with the phone itself
