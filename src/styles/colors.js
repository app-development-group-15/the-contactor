export const searchHeader = 'rgb(248, 248, 248)';
export const backgroundRow = 'rgb(255, 255, 255)';
export const userPhotoHeader = 'rgb(251, 250, 255)';
export const searchBoxDisabled = 'rgb(235, 235, 236)';
export const searchBoxEnabled = 'rgb(231, 231, 233)';
export const button = 'rgb(0, 122, 255)';
export const addPhoto = 'rgb(137, 142, 154)';
export const contactNumber = 'rgb(196, 196, 199)';

export const editContactBackground = 'rgb(241, 241, 245)';
export const iosBlue = 'rgb(0, 122, 255)';
export const iosFadedText = 'rgb(145, 145, 151)';
