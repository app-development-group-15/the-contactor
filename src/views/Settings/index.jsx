import React from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import styles from './styles';
import { deleteAllContacts, importContacts } from '../../services/fileService';

class Settings extends React.Component {
  componentDidMount() {
    this.props.navigation.setOptions({
      title: 'Settings',
    });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <TouchableHighlight
          style={styles.settingsItem}
          onPress={() => deleteAllContacts(this.props.navigation)}
        >
          <Text>Delete all contacts</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.settingsItem}
          onPress={() => importContacts(this.props.navigation)}
        >
          <Text>Import local contacts</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
export default Settings;
