import { StyleSheet } from 'react-native';
import { button, searchHeader } from '../../styles/colors';

export default StyleSheet.create({
  header: {
    backgroundColor: searchHeader,
    height: 10,
  },
  button: {
    paddingRight: 15,
    paddingLeft: 15,
    paddingBottom: 10,
    color: button,
  },
});
