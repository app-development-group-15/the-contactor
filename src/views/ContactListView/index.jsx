import React from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import SearchBar from 'react-native-platform-searchbar';
import { Ionicons } from '@expo/vector-icons';
import ContactList from '../../components/ContactList';
import styles from './styles';
import { readAllContacts } from '../../services/fileService';

class ContactListView extends React.Component {
  constructor(props){
    super(props)
    this.state = {
        search: '',
        loaded: true,
      }
    this.allContacts = [];
  }


  componentDidMount(){
    this.props.navigation.setOptions({
      title: 'Contacts',
      headerRight: () => (
        <TouchableHighlight onPress={() => this.props.navigation.navigate('CreateContact') }>
          <Ionicons
            style={styles.button}
            name="ios-add"
            size={30}
          />
        </TouchableHighlight>
      ),
      headerLeft: () => (
        <TouchableHighlight onPress={() => this.props.navigation.navigate('Settings')}>
          <Ionicons
            style={styles.button}
            name="ios-settings"
            size={30}
          />
        </TouchableHighlight>
      ),
    });
    this.loadContacts();
  }
  loadContacts = async () => {
    this.allContacts = await readAllContacts();
    console.log('contacts have been loaded')
    this.setState({loaded: true});
  }
  getContacts = () => {
    const { search } = this.state;
    const contacts = this.allContacts.filter((contact) => (
      contact.name.toLowerCase().includes(search.toLowerCase())));
    return contacts.sort((a, b) => (a.name < b.name ? -1 : (a.name) > b.name));
  };

  updateSearch = (search) => {
    this.setState({ search });
  }

  render() {
    if (!this.state.loaded) {
      return <View></View>
    }
    const { search } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <SearchBar
          placeholder="Search contacts"
          onChangeText={this.updateSearch}
          value={search}
        />
        <ContactList contacts={this.getContacts()} />
      </View>
    );
  }
}
export default ContactListView;
