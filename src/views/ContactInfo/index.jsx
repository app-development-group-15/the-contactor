import React from 'react';
import { View } from 'react-native';
import ContactInfoCard from '../../components/ContactInfoCard';

// import data from '';

// eslint-disable-next-line react/prefer-stateless-function
class ContactInfo extends React.Component {
  /*   constructor(props) {
    super(props);
    // console.log(this.props.route);
  }
 */
  /* componentDidMount() {
    // eslint-disable-next-line react/prop-types
    this.props.navigation.setOptions({
      title: 'ContactInfo',
    });
  } */

  render() {
    const { contact } = this.props.route.params;
    const { navigation } = this.props;
    console.log('CONTACT IN CONTACTINFO', contact);
    return (
      <View style={{ flex: 1 }}>
        <ContactInfoCard contact={contact} navigation={navigation} />
      </View>
    );
  }
}

const hello = 'jsx';

export default ContactInfo;
