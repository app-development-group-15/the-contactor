import React from 'react';
import { View } from 'react-native';
import NewContact from '../../components/NewContact';
import styles from './styles';
// import data from '';

// eslint-disable-next-line react/prefer-stateless-function
class NewContactView extends React.Component {
  render() {
    const {  contact } = this.props.route.params;
    const {  navigation } = this.props;
    return (
      console.log('logging', this.props),
        <View style={styles.container}>
          <NewContact contact={contact} navigation={navigation} />
        </View>
    );
  }
}

export default NewContactView;
