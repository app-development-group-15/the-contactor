import * as FileSystem from 'expo-file-system';
import { v4 as uuid } from 'uuid';
import * as Contacts from 'expo-contacts';

const setupDirectory = async (inputDir) => {
  const dir = await FileSystem.getInfoAsync(inputDir);
  if (!dir.exists) {
    await FileSystem.makeDirectoryAsync(inputDir);
  }
};

const createId = (contact) => {
  contact.id = uuid();
};

const contactsDir = `${FileSystem.documentDirectory}Contacts`;

const getFileUri = (fileName) => `${contactsDir}/${fileName}`;

// makes the name lowercase with - instead of spaces and removes all special characters
const getSlug = (string) => string.toLowerCase().replace(' ', '-').replace(/[^a-zA-Z-]/g, '');

export const updateContact = async (contact, oldContact, navigation) => {
  // Deletes contact
  const slug = getSlug(oldContact.name);
  FileSystem.deleteAsync(`${contactsDir}/${slug}-${oldContact.id}.json`);
  // Creates a new contact
  writeNewContact(contact);
  navigation.reset({
    index: 0,
    routes: [{ name: 'ContactListView' }],
  });

}  


export const deleteAllContacts = async (navigation) => {
  const dir = await FileSystem.getInfoAsync(contactsDir);
  if (dir.exists) {
    FileSystem.deleteAsync(contactsDir);
    setupDirectory(contactsDir);
    navigation.reset({
      index: 0,
      routes: [{ name: 'ContactListView' }],
    });
  }
};

export const writeNewContact = async (contact) => {
  setupDirectory(contactsDir);
  createId(contact);
  const slug = getSlug(contact.name);
  FileSystem.writeAsStringAsync(
    `${contactsDir}/${slug}-${contact.id}.json`,
    JSON.stringify(contact),
  );
};

export const importContacts = async (navigation) => {
  const { status } = await Contacts.requestPermissionsAsync();
  if (status === 'granted') {
    const { data } = await Contacts.getContactsAsync({
      fields: [Contacts.Fields.PhoneNumbers],
    });
    data.forEach((contact, i) => {
      if (contact.phoneNumbers !== undefined) {
        writeNewContact({
          id: uuid(),
          name: contact.name,
          phoneNumber: contact.phoneNumbers[0].number,
          thumbnailPhoto: '',
        });
      }
    });
    navigation.reset({
      index: 0,
      routes: [{ name: 'ContactListView' }],
    });
  }
};

export const deleteContact = async (contact, navigation) => {
  const slug = getSlug(contact.name);
  FileSystem.deleteAsync(`${contactsDir}/${slug}-${contact.id}.json`);
  navigation.reset({
    index: 0,
    routes: [{ name: 'ContactListView' }],
  });
};

export const readAllContacts = async () => {
  setupDirectory(contactsDir);
  const allContactFiles = await FileSystem.readDirectoryAsync(contactsDir);
  const allContacts = [];
  // TODO: change to a array iterator, the linter is unhappy with for of loops
  for (const fileName of allContactFiles) {
    const contact = await FileSystem.readAsStringAsync(getFileUri(fileName));
    allContacts.push(JSON.parse(contact));
  }
  return allContacts;
};
