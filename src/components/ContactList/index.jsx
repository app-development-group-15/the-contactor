import React from 'react';
import {
  View, FlatList, TextInput,
} from 'react-native';
import ContactListItem from '../ContactListItem';
// import styles from '../ContactListItem/styles';

const ContactList = ({ contacts }) => (
  <View style={{ flex: 1 }}>
    <FlatList
      numColumns={1}
      data={contacts}
      renderItem={({ item }) => (
        <ContactListItem item={item} />
      )}
      keyExtractor={(contact) => contact.id.toString()}
    />
  </View>
);

export default ContactList;
