import React from 'react';
import {
  View, Text, Image, TouchableHighlight, Button,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import call from 'react-native-phone-call';
import styles from './styles';

class ContactInfoCard extends React.Component {
  componentDidMount() {
    console.log('contact', this.props.contact);
    this.props.navigation.setOptions({
      title: '',
    });
  }

  render() {
    const { contact } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.photoBox}>
          {
          (contact.thumbnailPhoto !== '')
            ? (
              <Image
                source={{ uri: contact.thumbnailPhoto }}
                style={styles.photo}
              />
            )
            : (
              <View style={styles.aInsideCircle}>
                <Text style={styles.aInsideCircleText}>{contact.name[0]}</Text>
              </View>
            )

        }
          <Text style={styles.name}>{contact.name}</Text>
        </View>
        <View style={styles.teleDetails}>
          <Text style={styles.teleHeadline}>mobile</Text>
          <Text style={styles.teleNumber}>
            {contact.phoneNumber}
          </Text>
          <Text style={styles.borderLine} />
          <TouchableHighlight
            style={styles.phoneIconWrapper}
            onPress={() => call({ number: contact.phoneNumber, prompt: true })}
          >
            <Ionicons name="ios-call" size={60} style={styles.phoneIcon} />
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}
export default ContactInfoCard;
