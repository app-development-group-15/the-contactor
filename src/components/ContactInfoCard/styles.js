import { StyleSheet, Dimensions } from 'react-native';
import {  addPhoto, iosBlue } from '../../styles/colors';
const { width: winWidth } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    width: winWidth,
  },
  photoBox: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#FCFAFF',
    // backgroundColor: '#bcbbc1',
    backgroundColor: '#f2f1f6',
    borderBottomColor: '#bcbbc1',
    borderBottomWidth: 1,
  },
  photo: {
    width: winWidth / 2,
    height: winWidth / 2,
    borderRadius: winWidth / 4,
    overflow: 'hidden',
  },
  name: {
    marginTop: 30,
    fontSize: 40,
  },
  teleDetails: {
    backgroundColor: 'white',
    flex: 2,
  },
  teleHeadline: {
    marginTop: 5,
    marginLeft: 10,
    backgroundColor: 'white',
  },
  teleNumber: {
    fontSize: 20,
    color: '#007aff',
    backgroundColor: 'white',
    marginTop: 5,
    marginLeft: 10,
    alignItems: 'flex-start',
  },
  borderLine: {
    width: '100%',
    height: 1,
    marginTop: 10,
    backgroundColor: '#bcbbc1',
    alignSelf: 'center',
  },
  phoneIconWrapper: {
    marginTop: '25%',
    alignSelf: 'center',
    width: 85,
    height: 85,
    backgroundColor: '#4DD964',
    borderRadius: 50,
  },
  phoneIcon: {
    color: 'white',
    justifyContent: 'center',
    marginTop: 12,
    alignSelf: 'center',
  },
  textExtra: {
    flex: 1,
    marginTop: 10,
    alignSelf: 'center',
    fontWeight: '200',
  },
  aInsideCircle: {
    justifyContent: 'center',
    backgroundColor: addPhoto,
    width: winWidth / 2,
    height: winWidth / 2,
    borderRadius: winWidth / 4,
  },
  aInsideCircleText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 100,
  },
  addPhotoText: {
    color: iosBlue,
    fontSize: 20,
    marginTop: 30,
  },
});
