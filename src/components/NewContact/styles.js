import { StyleSheet, Dimensions } from 'react-native';
import {
  addPhoto, contactNumber, editContactBackground, iosBlue, iosFadedText
} from '../../styles/colors';

const { width: winWidth } = Dimensions.get('window');

// const addPhoto = 'rgb(137, 142, 154)';
// const contactNumber = 'rgb(196, 196, 199)';

// const editContactBackground = 'rgb(241, 241, 245)';
// const iosBlue = 'rgb(0, 122, 255)';
// const iosFadedText = 'rgb(145, 145, 151)';

export default StyleSheet.create({
  container: {
    flex: 1,
    width: winWidth,
    backgroundColor: editContactBackground,
    justifyContent: 'center',
  },
  photoArea: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  photoContainer: {
    width: winWidth / 2,
    height: winWidth / 2,
    borderRadius: winWidth / 4,
    backgroundColor: addPhoto,
  },
  aInsideCircle: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: addPhoto,
    width: winWidth / 2,
    height: winWidth / 2,
    borderRadius: winWidth / 4,
  },
  aInsideCircleText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 100,
  },
  addPhotoText: {
    color: iosBlue,
    fontSize: 20,
    marginTop: 30,
  },
  detailsArea: {
    flex: 1,
  },
  textInput: {
    fontSize: 20,
    color: iosFadedText,
    backgroundColor: 'white',
    width: winWidth,
    paddingLeft: 20,
    paddingTop: 12,
    paddingBottom: 10,
    // border bottom does not work with text components
  },
  textInputActive: {
    fontSize: 20,
    // color: iosFadedText,
    backgroundColor: 'white',
    width: winWidth,
    paddingLeft: 20,
    paddingTop: 12,
    paddingBottom: 10,
    // border bottom does not work with text components
    // only diff btw input & active is color
    color: 'black',
  },
  border: {
    width: winWidth - 20,
    height: 0.5,
    backgroundColor: contactNumber,
    marginLeft: 10,
  },
  photo: {
    width: winWidth / 2,
    height: winWidth / 2,
    borderRadius: winWidth / 4,
  },
});
