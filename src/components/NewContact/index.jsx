import React from 'react';
import {
  View, Text, TextInput, Image, TouchableHighlight, Button,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import PropTypes from 'prop-types';
import styles from './styles';
import { writeNewContact, updateContact } from '../../services/fileService';
import { takePhoto, selectFromCameraRoll } from '../../services/imageService';
import AddModal from '../AddModal';
// eslint-disable-next-line react/prefer-stateless-function
class NewContact extends React.Component {
  constructor(props) {
    super(props);
    if (this.props.contact !== undefined) {
      // Set props to variables
      const {
        name, phoneNumber, thumbnailPhoto, id,
      } = this.props.contact;
      this.state = {
        name: name || '',
        phoneNumber: phoneNumber || '',
        thumbnailPhoto: thumbnailPhoto || '',
        id: id || '',
        isAddModalOpen: false,
      };
    } else {
      // Contact is undefined
      this.state = {
        name: '',
        phoneNumber: '',
        thumbnailPhoto: '',
        id: '',
        isAddModalOpen: false,
      };
    }

    this.navigation = props.navigation;
  }

  componentDidMount() {
    if (this.props.contact !== undefined) {
      this.id = { id } = this.props.contact;
      this.header = 'Edit contact';
    } else {
      this.header = 'New Contact';
    }
    this.navigation.setOptions({
      // Edit or New contact not working
      title: this.header,
      headerRight: () => (
        <Button
          onPress={() => { (this.id === undefined) ? this.saveContact() : this.updateContact(); }}
          title="Done"
        />
      ),
      headerLeft: () => (
        <Button
          onPress={() => this.navigation.goBack()}
          title="Cancel"
        />
      ),
    });
  }

  async takePhoto() {
    const photo = await takePhoto();
    if (photo.length > 0) { await this.addImage(photo); }
  }

  async selectFromCameraRoll() {
    const photo = await selectFromCameraRoll();
    if (photo.length > 0) { await this.addImage(photo); }
  }

  async addImage(image) {
    this.setState({ thumbnailPhoto: image, isAddModalOpen: false });
  }

  updateContact() {
    // Delete and create new contact
    const { contact: oldContact, navigation } = this.props;
    const { name, phoneNumber, thumbnailPhoto } = this.state;
    const newContact = {
      id,
      name,
      phoneNumber,
      thumbnailPhoto,
    };
    updateContact(newContact, oldContact, navigation);
  }

  saveContact() {
    const { name, phoneNumber, thumbnailPhoto } = this.state;
    const contact = {
      id: 0,
      name,
      phoneNumber,
      thumbnailPhoto,
    };
    writeNewContact(contact);
    this.navigation.reset({
      index: 0,
      routes: [{ name: 'ContactListView' }],
    });
  }

  render() {
    const {
      thumbnailPhoto, name, id, phoneNumber, isAddModalOpen,
    } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.photoArea}>
          <View style={styles.photoContainer}>
            {(thumbnailPhoto === '')
              ? (
                <View style={styles.aInsideCircle}>
                  <Text style={styles.aInsideCircleText}>{name[0]}</Text>
                </View>
              )
              : (
                <View>
                  <Image
                    style={styles.photo}
                    source={{ uri: thumbnailPhoto }}
                  />
                </View>
              )}
          </View>
          <TouchableHighlight
            onPress={() => this.setState({ isAddModalOpen: true })}
          >
            <Text style={styles.addPhotoText}>Add Photo</Text>
          </TouchableHighlight>
        </View>
        <View style={styles.detailsArea}>
          <TextInput
            style={styles.textInput}
            onChangeText={(name) => this.setState({ name })}
            defaultValue={name}
            placeholder="Name"
            ref={(el) => { this.name = el; }}
          />
          <View style={styles.border} />
          <TextInput
            onChangeText={(phoneNumber) => this.setState({ phoneNumber })}
            defaultValue={phoneNumber}
            style={styles.textInput}
            placeholder="Phone number"
            ref={(el) => { this.phoneNumber = el; }}
            onChangeText={(phoneNumber) => this.setState({ phoneNumber })}
            keyboardType='number-pad'
            maxLength={10}
          />
          <View style={styles.border} />
          <AddModal
            isOpen={isAddModalOpen}
            closeModal={() => this.setState({ isAddModalOpen: false })}
            takePhoto={() => this.takePhoto()}
            selectFromCameraRoll={() => this.selectFromCameraRoll()}
          />
        </View>
      </View>

    );
  }
}
// export default NewContact;
export default function ret({ contact }) {
  const navigation = useNavigation();
  return <NewContact navigation={navigation} contact={contact} />;
}
