import React from 'react';
import {
  View, Text, TouchableHighlight, Image, Alert,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import styles from './styles';
import { deleteContact } from '../../services/fileService';
import { editContactBackground } from '../../styles/colors';

function onListLongPress(contact, navigation) {
  console.log(contact);
  Alert.alert(
    'Do You want to delete this contact?',
    'This action cannot be undone',
    [
      {
        text: 'Cancel',
        onpress: () => console.log('Item not deleted'),
        style: 'cancel',
      },
      { text: 'Delete', onPress: () => deleteContact(contact, navigation) },
      { text: 'Edit', onPress: () => navigation.navigate('EditContact', { contact }) },
    ],
    { cancelable: false },
  );
}
const ContactListItem = (props) => {
  const navigation = useNavigation();
  const { item } = props;
  return (
  // On press move to edit page
    <TouchableHighlight
      onPress={() => navigation.navigate('ContactDetails', { contact: item })}
      onLongPress={() => onListLongPress(item, navigation)}
    >
      <View style={styles.container}>
        <View style={styles.subContainer}>
          {
            (item.thumbnailPhoto !== '')
              ? <Image style={styles.image} source={{ uri: item.thumbnailPhoto }} resizeMode="cover" />
              : (
                <View style={styles.aInsideCircle}>
                  <Text style={styles.aInsideCircleText}>{item.name[0]}</Text>
                </View>
              )
          }

        </View>
        <View>
          <Text style={styles.name} numberOfLines={1}>
            {item.name}
            {' '}
          </Text>
          <Text>{item.number}</Text>
        </View>
      </View>
    </TouchableHighlight>
  );
};

export default ContactListItem;
