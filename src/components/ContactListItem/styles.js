import { StyleSheet } from 'react-native';
import { backgroundRow, addPhoto } from '../../styles/colors';

export default StyleSheet.create({
    image: {
        width: 60,
        height: 60,
        borderRadius: 100,
        paddingLeft: 10,
    },
    container: {
        marginTop: 1,
        flexDirection: 'row',
        paddingLeft: 8,
        paddingTop: 8,
        paddingBottom: 10,
        alignItems: 'center',
        backgroundColor: backgroundRow,
    },
    name: {
        fontSize: 24,
        paddingLeft: 10,
        alignItems: 'center',
    },
    aInsideCircle: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: addPhoto,
        width: 60,
        height: 60,
        borderRadius: 100,
    }, 
    aInsideCircleText: {
        color: 'white',
        textAlign: 'center',
        fontSize: 20,
    },
    subContainer: {
        alignItems: 'center',
        justifyContent: 'center',
    }
})
