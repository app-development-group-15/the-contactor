import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import ContactListView from '../views/ContactListView';
import ContactInfo from '../views/ContactInfo';
import NewContactView from '../views/NewContactView';
import EditContact from '../views/EditContact';
import Settings from '../views/Settings';

const Stack = createStackNavigator();

function AppContainer() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="ContactListView" component={ContactListView} />
        <Stack.Screen name="ContactDetails" component={ContactInfo} />
        <Stack.Screen name="CreateContact" component={NewContactView} />
        <Stack.Screen name="EditContact" component={EditContact} />
        <Stack.Screen name="Settings" component={Settings} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default AppContainer;
